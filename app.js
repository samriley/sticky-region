var StickyRegion = require('./src/sticky-region');

StickyRegion.init('#example-header', {header: 'header'});
StickyRegion.init('#example-both', {header: 'header', footer: 'footer'});
StickyRegion.init('#example-footer', {footer: 'footer'});