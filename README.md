# Simple Sticky Header/Footer

As the page scrolls this script will lock a header/footer/both to the appropriate location.

## Usage

```
<div id="example-both">
    <header>Lorem Ipsum</header>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium blanditiis cupiditate deserunt dolor dolore doloremque eaque eveniet explicabo fugit libero, nam nesciunt perspiciatis provident quaerat quo ratione reprehenderit sit?</p>
    <footer>Lorem Ipsum</footer>
</div>

<script>
var StickyRegion = require('sticky-region');

StickyRegion.init('#example-both', {header: 'header', footer: 'footer'});
</script>
```