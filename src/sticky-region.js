StickyRegion = {};

StickyRegion.opts = {};

StickyRegion.widget = function(region, opts) {
  this.region = document.querySelector(region);

  if (opts.header) {
    this.headerEl = this.region.querySelector(opts.header);
    this.headerEl.classList.add('sticky-region');
  }

  if (opts.footer) {
    this.footerEl = this.region.querySelector(opts.footer);
    this.footerEl.classList.add('sticky-region');
  }

  window.addEventListener('scroll', this.debounce(this._handleScroll).bind(this));
  window.addEventListener('resize', this.debounce(this._handleScroll).bind(this));
};

StickyRegion.widget.prototype.debounce = function(func, duration) {
  return function() {
    if (this.timer) {
      return;
    }
    duration = duration || 200;

    this.timer = setTimeout(function() {
      func.apply(this, arguments);
      this.timer = null;
    }.bind(this), duration);
  }.bind(this);
};

StickyRegion.widget.prototype._handleScroll = function() {
  if (this.headerEl) {
    this._handleStickyHeader();
  }
  if (this.footerEl) {
    this._handleStickyFooter();
  }
};

StickyRegion.widget.prototype._handleStickyHeader = function() {
  // TODO: Remove this shim when IE9 compatibility is no longer required.
  var scrollY = window.scrollY || window.pageYOffset;

  if (scrollY < this.region.offsetTop) {
    this.headerEl.classList.remove('sticky-region-fixed', 'sticky-region-hidden');
    this.headerEl.style.top = null;
    this.region.style.paddingTop = null;
  } else if (scrollY > this.headerEl.offsetTop) {
    this.headerEl.classList.add('sticky-region-fixed');
    this.headerEl.style.top = 0;
    this.region.style.paddingTop = this.headerEl.offsetHeight + 'px';

    if (scrollY > this.region.offsetTop + this.region.offsetHeight - this.headerEl.offsetHeight) {
      this.headerEl.classList.add('sticky-region-hidden');
    } else {
      this.headerEl.classList.remove('sticky-region-hidden');
    }
  }
};

StickyRegion.widget.prototype._handleStickyFooter = function() {
  // TODO: Remove this shim when IE9 compatibility is no longer required.
  var scrollY = window.scrollY || window.pageYOffset;

  if (scrollY + window.innerHeight < this.region.offsetTop + this.footerEl.offsetHeight) {
    this.footerEl.classList.add('sticky-region-hidden');
  } else {
    this.footerEl.classList.remove('sticky-region-hidden');
    if (scrollY + window.innerHeight > this.region.offsetTop + this.region.offsetHeight) {
      this.footerEl.classList.remove('sticky-region-fixed');
      this.footerEl.style.bottom = null;
      this.region.style.paddingBottom = null;
    } else if (scrollY + window.innerHeight > this.region.offsetTop + this.footerEl.offsetHeight) {
      this.footerEl.classList.add('sticky-region-fixed');
      this.footerEl.style.bottom = 0;
      this.region.style.paddingBottom = this.footerEl.offsetHeight + 'px';
    }
  }
};

/**
 *
 * @param {Element} region
 * @param {Object} opts
 * @returns {StickyRegion.widget}
 */
StickyRegion.init = function(region, opts) {
  opts = opts || {};
  opts = Object.assign({}, StickyRegion.opts, opts);

  return new StickyRegion.widget(region, opts);
};

if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {
  define(function() {
    return StickyRegion;
  });
} else if (module && module.exports) {
  module.exports = StickyRegion;
}
